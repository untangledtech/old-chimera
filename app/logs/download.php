<?php
	require_once "../../cogs/data.class.php";
	header( "Content-Disposition: attachment; filename=\"logs." . date( "n" ) . "." . date( "j" ) . "." . date( "y" ) . ".txt\"" );
	header( "Content-Type: application/force-download" );
	
	$data = new data( );
	$ltable = $data->getd( "logs" );
	foreach( $ltable as $id => $table ) {
		echo( "[ $id ]" );
		if ( $table[ "type" ] == "info" ) {
			echo( "[ Info ]" );
		} elseif ( $table[ "type" ] == "error" ) {
			echo( "[ Error ]" );
		} elseif ( $table[ "type" ] == "warning" ) {
			echo( "[ Warning ]" );
		}
		echo( "[ " . $table[ "date" ] . " ] >> " );
		echo( $table[ "msg" ] );
		echo( "\r\n" );
	}
?>