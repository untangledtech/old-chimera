﻿<?php
	require_once "cogs/data.class.php";
	require_once "cogs/log.class.php";
	$log = new log( );
?>
<!DOCTYPE html>
<html>
<head>
	<title>Logs</title>
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="assets/css/bootstrap.chimera.css" rel="stylesheet" media="screen">
	<link href="assets/css/bootstrap-responsive.css" rel="stylesheet" media="screen">
	<link href="assets/css/tablecloth.css" rel="stylesheet">
	<style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
	</style>
	<link rel="icon" type="image/png" href="assets/img/square.ico">
</head>
<body>
	<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
			  <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="brand" href="/index" data-method="get">Chimera</a>
        	<div class="nav-collapse collapse">
				<ul class="nav">
				  <li class="">
					<a href="/index"><i class="icon-th-large"></i> Dashboard</a>
				  </li>
				  <li class="">
					<a href="/users"><i class="icon-user"></i> Users</a>
				  </li>
				  <li class="active">
					<a href="/logs"><i class="icon-book"></i> Logs</a>
				  </li>
				  <li class="">
					<a href="#forms"><i class="icon-list"></i> Forms</a>
				  </li>
				  <li class="">
					<a href="/settings"><i class="icon-cog"></i> Settings</a>
				  </li>
				</ul>
			</div>
          </div>
        </div>
      </div>
	<div class='container-fluid'>
	<div class='row-fluid'>
	<div class='span3'>
		<div class='well sidebar-nav'>
			<ul class='nav nav-list'><li class='nav-header'>Navigation → Logs</li>
            <li class="active">
              <a href="/logs">View logs</a>
            </li>
            <li>
              <a href="/logs/download">Download logs</a>
            </li>
            <li>
              <a href="/logs/clear">Clear logs</a>
            </li>
	</div>
</div>
<div class='span9'>
<div class='row-fluid'>
<div class='page-header'>
<h1>Logs</h1>
</div>
<ul class="breadcrumb"><li class=""><a href="index" class="pjax">Dashboard</a></li><span class="divider"> / </span><li class="active"><a href="/logs" class="pjax">Logs</a></li></ul>
<ul class='nav nav-tabs'>

          <li title="" rel="" class="icon index_collection_link active">
            <a href="/logs">
              <i class="icon-th-list"></i>
              <span>Logs</span>
            </a>
          </li>
		  
		  <li title="" rel="" class="icon export_collection_link ">
            <a href="/logs/download">
              <i class="icon-arrow-down"></i>
              <span>Download</span>
            </a>
          </li>

          <li title="" rel="" class="icon index_collection_link">
            <a href="/logs/clear">
              <i class="icon-remove"></i>
              <span>Clear</span>
            </a>
          </li>
		  </ul>
<table id="logs" class="table table-condensed">
<thead>
<tr>
<th>ID</th>
<th>Level</th>
<th>Date/Time</th>
<th>Message</th>
</tr>
</thead>
<tbody>
<?php
	$data = new data( );
	$ltable = $data->getd( "logs" );
	foreach( $ltable as $id => $table ) {
		echo( "<tr>" );
		echo( "<td>" . $id . "</td>" );
		if ( $table[ "type" ] == "info" ) {
			echo( "<td><span class=\"label label-info\">Info</span></td>" );
		} elseif ( $table[ "type" ] == "error" ) {
			echo( "<td><span class=\"label label-important\">Error</span></td>" );
		} elseif ( $table[ "type" ] == "warning" ) {
			echo( "<td><span class=\"label label-warning\">Warning</span></td>" );
		}
		echo( "<td>" . $table[ "date" ] . "</td>" );
		echo( "<td>" . $table[ "msg" ] . "</td>" );
		echo( "</tr>" );
	}
?>
</tbody>
</table>
	</div>
	</div>
	</div>
	</div>
	<div class="label label-info" id="loading" style="position:fixed; right:20px; bottom:20px; z-index:100000">Loading...</div>
	<script src="http://code.jquery.com/jquery-latest.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.metadata.js"></script>
	<script src="assets/js/jquery.tablesorter.min.js"></script>
	<script src="assets/js/jquery.tablecloth.js"></script>
	<script>
		$(window).load(function(){
			$('#loading').fadeOut();
		});
	</script>
	<script type="text/javascript" charset="utf-8">
      $(document).ready(function() {
        $("table").tablecloth({
          theme: "default",
          striped: true,
		  clean: true,
          sortable: true,
          condensed: true
        });
      });
	$(function() {
		$("table#logs").tablesorter({ sortList: [[2,1]] });
	});
    </script>
</body>
</html>