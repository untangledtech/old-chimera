<!DOCTYPE html>
<html>
<head>
	<title>Dashboard</title>
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="assets/css/bootstrap.chimera.css" rel="stylesheet" media="screen">
	<link href="assets/css/bootstrap-responsive.css" rel="stylesheet" media="screen">
	<style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
	</style>
	<link rel="icon" type="image/png" href="assets/img/square.ico">
</head>
<body>
	<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
			  <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="brand" href="/index" data-method="get">Chimera</a>
			  <div class="nav-collapse collapse">
				<ul class="nav">
				  <li class="active">
					<a href="index"><i class="icon-th-large"></i> Dashboard</a>
				  </li>
				  <li class="">
					<a href="users"><i class="icon-user"></i> Users</a>
				  </li>
				  <li class="">
					<a href="/logs"><i class="icon-book"></i> Logs</a>
				  </li>
				  <li class="">
					<a href="#forms"><i class="icon-list"></i> Forms</a>
				  </li>
				  <li class="">
					<a href="settings"><i class="icon-cog"></i> Settings</a>
				  </li>
				</ul>
			</div>
          </div>
        </div>
      </div>
	<div class='container-fluid'>
	<div class='row-fluid'>
	
	<div class='span3'>
		<div class='well sidebar-nav'>
			<ul class='nav nav-list'><li class='nav-header'>Navigation</li>
            <li data-model="ball">
              <a href="ball">Balls</a>
            </li>
            <li data-model="comment">
              <a href="comment">Comments</a>
            </li>
            <li data-model="division">
              <a href="division">Divisions</a>
            </li>
            <li data-model="draft">
              <a href="draft">Drafts</a>
            </li>
            <li data-model="fan">
              <a href="fan">Fans</a>
            </li>
            <li data-model="field_test">
              <a href="field_test">Field tests</a>
            </li>
            <li data-model="league">
              <a href="league">Leagues</a>
            </li>
            <li data-model="nested_field_test">
              <a href="nested_field_test">Nested field tests</a>
            </li>
            <li data-model="player">
              <a href="player">Players</a>
            </li>
            <li data-model="team">
              <a href="team">Teams</a>
            </li>
            <li data-model="user">
              <a href="user">Users</a>
            </li>
          <li class='nav-header'>Cms</li>
            <li data-model="cms~basic_page">
              <a href="cms~basic_page">Basic pages</a>
            </li>
            
          </ul>
	</div>
</div>
<div class='span9'>
<div class='row-fluid'>
<div class='page-header'>
	<h1>Dashboard</h1>
</div>
<ul class="breadcrumb"><li class="active"><a href="/index" >Dashboard</a></li></ul>
<ul class='nav nav-tabs'>
          <li title="" rel="" class="icon dashboard_root_link active">
            <a href="">
              <i class="icon-home"></i>
              <span>Dashboard</span>
            </a>
          </li>
</ul>
	</div>
	</div>
	</div>
	</div>
	<div class="label label-info" id="loading" style="position:fixed; right:20px; bottom:20px; z-index:100000">Loading...</div>
	<script src="http://code.jquery.com/jquery-latest.js"></script>
	<script>
		$(window).load(function(){
			$('#loading').fadeOut();
		});
	</script>
</body>
</html>