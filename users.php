﻿<?php
	require_once "cogs/data.class.php";
	require_once "cogs/auth.class.php";
	//require_once "cogs/log.class.php";
	$data = new data( );
	$auth = new auth( );
?>
<!DOCTYPE html>
<html>
<head>
	<title>Manage Users</title>
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="assets/css/bootstrap.chimera.css" rel="stylesheet" media="screen">
	<link href="assets/css/bootstrap-responsive.css" rel="stylesheet" media="screen">
	<link href="assets/css/tablecloth.css" rel="stylesheet">
	<style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
	</style>
	<link rel="icon" type="image/png" href="assets/img/square.ico">
</head>
<body>
	<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
			  <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="brand" href="/index" data-method="get">Chimera</a>
        	<div class="nav-collapse collapse">
				<ul class="nav">
				  <li class="">
					<a href="/index"><i class="icon-th-large"></i> Dashboard</a>
				  </li>
				  <li class="active">
					<a href="/users"><i class="icon-user"></i> Users</a>
				  </li>
				  <li class="">
					<a href="/logs"><i class="icon-book"></i> Logs</a>
				  </li>
				  <li class="">
					<a href="#forms"><i class="icon-list"></i> Forms</a>
				  </li>
				  <li class="">
					<a href="/settings"><i class="icon-cog"></i> Settings</a>
				  </li>
				</ul>
			</div>
          </div>
        </div>
      </div>
	<div class='container-fluid'>
	<div class='row-fluid'>
	<div class='span3'>
		<div class='well sidebar-nav'>
			<ul class='nav nav-list'><li class='nav-header'>Navigation → Manage Users</li>
            <li class="active">
              <a href="/users">List users</a>
            </li>
            <li>
              <a href="/logs/download">Download logs</a>
            </li>
            <li>
              <a href="/logs/clear">Clear logs</a>
            </li>
	</div>
</div>
<div class='span9'>
<div class='row-fluid'>
<div class='page-header'>
<h1>Manage Users</h1>
</div>
<ul class="breadcrumb"><li class=""><a href="index" class="pjax">Dashboard</a></li><span class="divider"> / </span><li class="active"><a href="/users" class="pjax">Users</a></li></ul>
<ul class='nav nav-tabs'>

          <li title="" rel="" class="icon index_collection_link active">
            <a href="/users">
              <i class="icon-th-list"></i>
              <span>List</span>
            </a>
          </li>
		  
		  <li title="" rel="" class="icon export_collection_link ">
            <a href="/users/download">
              <i class="icon-arrow-down"></i>
              <span>Download</span>
            </a>
          </li>

          <li title="" rel="" class="icon index_collection_link">
            <a href="/users/clear">
              <i class="icon-remove"></i>
              <span>Clear</span>
            </a>
          </li>
		  </ul>
<table id="logs" class="table table-condensed">
<thead>
<tr>
<th>Username</th>
<th>Flags</th>
</tr>
</thead>
<tbody>
<?php
	$utable = $data->getd( "users" );
	foreach( $utable as $user => $table ) {
		$flags = $table[ "flags" ];
		echo( "<tr>" );
		echo( "<td><span class=\"badge\">" . $user . "</span></td>" );
		if ( strlen( $flags ) > 0 ) {
			echo( "<td>" . str_replace( "admin", "<span class=\"label label-info\">admin</span>", str_replace( "immune", "<span class=\"label label-important\">immune</span>", str_replace( ",", " ", $flags ) ) ) . "</td>" );
		} else {
			echo( "<td><span class=\"label\">none</span></td>" );
		}
		if ( !$auth->has_flag( $user, "immune" ) ) {
			echo( @"
			<td class=\"last links\"><ul class='inline'>
				<li title=\"Show\" rel=\"tooltip\" class=\"icon show_member_link \">
					<a class=\"pjax\" href=\"/admin/league/101\">
					  <i class=\"icon-info-sign\"></i>
					</a>
				  </li>
			  <li title=\"Edit\" rel=\"tooltip\" class=\"icon edit_member_link \">
				<a class=\"pjax\" href=\"/admin/league/101/edit\">
				  <i class=\"icon-pencil\"></i>
				</a>
			  </li>
			
			  <li title=\"Delete\" rel=\"tooltip\" class=\"icon delete_member_link \">
				<a class=\"pjax\" href=\"/admin/league/101/delete\">
				  <i class=\"icon-remove\"></i>
				</a>
			  </li>
			</ul></td>" );
		} else {
			echo( @"
				<td class=\"last links\"><ul class='inline'>
				<li title=\"Show\" rel=\"tooltip\" class=\"icon show_member_link \">
					<a class=\"pjax\" href=\"/admin/league/101\">
					  <i class=\"icon-info-sign\"></i>
					</a>
				  </li>
					</ul></td>
				" );
		}
		echo( "</tr>" );
	}
?>
</tbody>
</table>
	</div>
	</div>
	</div>
	</div>
	<div class="label label-info" id="loading" style="position:fixed; right:20px; bottom:20px; z-index:100000">Loading...</div>
	<script src="http://code.jquery.com/jquery-latest.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script src="assets/js/jquery.metadata.js"></script>
	<script src="assets/js/jquery.tablesorter.min.js"></script>
	<script src="assets/js/jquery.tablecloth.js"></script>
	<script>
		$(window).load(function(){
			$('#loading').fadeOut();
		});
	</script>
	<script type="text/javascript" charset="utf-8">
      $(document).ready(function() {
        $("table").tablecloth({
          theme: "default",
          striped: true,
		  clean: true,
          sortable: true,
          condensed: true
        });
      });
	$(function() {
		$("table#logs").tablesorter({ sortList: [[2,1]] });
	});
    </script>
</body>
</html>