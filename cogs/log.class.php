<?php
	require_once "cogs/data.class.php";
	class log {
		private $data = null;
		
		public function __construct( ) {
			$this->data = new data( );
		}
		
		public function w( $level, $msg ) {
			$this->data->put( count( $this->data->getd( "logs" ) ) + 1, array(
					"date" => date( "F" ) . " " . date( "j" ) . ", " . date( "Y" ) . " " . date( "g" ) . ":" . date( "i" ) . ":" . date( "s" ),
					"type" => $level,
					"msg" => $msg
				),
			"logs" );
		}
	}
?>