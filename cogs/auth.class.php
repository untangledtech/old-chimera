<?php
	require_once "data.class.php";
	class auth {
		private $_salt;
		private $data;

		public function __construct()
		{
			$this->_salt = "10KQETJIP7FUN3WG25SM";
			$this->data = new data( );
		}

		public function hash( $string, $algorithm = "sha256" )
		{
			return hash_hmac( $algorithm, $string, $this->_salt);
		}
		
		public function create_user( $user, $password, $flags )
		{
			if ( $this->data->domain_exists( "users" ) ) {
				$this->data->put( $user, array(
				"password" => $this->hash( $password ),
				"flags" => $flags
				), "users" );
			}
		}
		
		public function authenticate( $user, $password )
		{
			if ( $this->data->domain_exists( "users" ) ) {
				$usertable = $this->data->getd( "users" );
				if( $this->hash( $password ) == $usertable[ $user ][ "password" ] ) {
					return true;
				}
			}
			return false;
		}
		
		public function has_flag( $user, $flag )
		{
			if ( $this->data->domain_exists( "users" ) ) {
				$utable = $this->data->getd( "users" );
				$flagtable = explode( ",", $utable[ $user ][ "flags" ] );
				if( in_array( $flag, $flagtable ) ) {
					return true;
				}
			}
			return false;
		}
		
		public function random_string( $length = 25 )
		{
			$characters = "0123456789abcdefghijklmnopqrstuvwxyz";
			$string = "";
			
			for ( $p = 0; $p < $length; $p++ ) {
				$string .= $characters[ mt_rand( 0, strlen($characters ) ) ];
			}
			return $string;
		}
	}
?>