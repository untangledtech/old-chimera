﻿<?php
	require_once "cogs/log.class.php";
	//require_once "cogs/auth.class.php";
	
	$data = new data( );
		//$data->put( "db_user", "mayjia" );
		//$data->put( "1234567", "ERROR - Blah blah blah", "logs" );
		/*$data->put( "mayjia",
			array(
				"password" => "password123",
				"flags" => "admin",
				"salt" => "a34azsda4asd"
			),
			"users" );
			$data->put( "43",
			array(
				"date" => date( "F" ) . " " . date( "j" ) . ", " . date( "Y" ) . " " . date( "g" ) . ":" . date( "i" ),
				"user" => "admin@admin.com",
				"type" => "info",
				"msg" => "Loaded data.class.php"
			),
			"logs" );
		$user = $data->get( "db_user" );
		$edata = $data->get( "1234567", "logs" );
		$bdata = $data->get( "mayjia", "users" );
		$cdata = $data->get( "43", "logs" );*/
	//print_r( $data->getd( "logs" ) );
		/*$data->put( "44", array(
				"date" => date( "F" ) . " " . date( "j" ) . ", " . date( "Y" ) . " " . date( "g" ) . ":" . date( "i" ),
				"user" => "test@admin.com",
				"type" => "warning",
				"msg" => "Function blah deprecated"
			),
			"logs" );
			$data->put( "45", array(
				"date" => date( "F" ) . " " . date( "j" ) . ", " . date( "Y" ) . " " . date( "g" ) . ":" . date( "i" ),
				"user" => "test@admin.com",
				"type" => "error",
				"msg" => "ZLib broke line 49"
			),
			"logs" );*/
?>
<!DOCTYPE html>
<html>
<head>
	<title>Settings</title>
	<link href="assets/css/bootstrap.min.css" rel="stylesheet" media="screen">
	<link href="assets/css/bootstrap.chimera.css" rel="stylesheet" media="screen">
	<link href="assets/css/bootstrap-responsive.css" rel="stylesheet" media="screen">
	<style>
      body {
        padding-top: 60px; /* 60px to make the container go all the way to the bottom of the topbar */
      }
	</style>
	<link rel="icon" type="image/png" href="assets/img/square.ico">
</head>
<body>
	<div class="navbar navbar-fixed-top">
      <div class="navbar-inner">
        <div class="container">
			  <button type="button" class="btn btn-navbar" data-toggle="collapse" data-target=".nav-collapse">
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			  </button>
			  <a class="brand" href="/index" data-method="get">Chimera</a>
        	<div class="nav-collapse collapse">
				<ul class="nav">
				  <li class="">
					<a href="/index"><i class="icon-th-large"></i> Dashboard</a>
				  </li>
				  <li class="">
					<a href="/users"><i class="icon-user"></i> Users</a>
				  </li>
				  <li class="">
					<a href="/logs"><i class="icon-book"></i> Logs</a>
				  </li>
				  <li class="">
					<a href="#forms"><i class="icon-list"></i> Forms</a>
				  </li>
				  <li class="active">
					<a href="/settings"><i class="icon-cog"></i> Settings</a>
				  </li>
				</ul>
			</div>
          </div>
        </div>
      </div>
	<div class='container-fluid'>
	<div class='row-fluid'>
	<div class='span3'>
		<div class='well sidebar-nav'>
			<ul class='nav nav-list'><li class='nav-header'>Navigation → Manage Settings</li>
            <li  class="active" data-model="user">
              <a href="user">List users</a>
            </li>
            <li data-model="comment">
              <a href="comment">Add new user</a>
            </li>
            <li data-model="division">
              <a href="division">Export users</a>
            </li>
            <li data-model="draft">
              <a href="draft">History</a>
            </li>
	</div>
</div>
<div class='span9'>
<div class='row-fluid'>
<div class='page-header'>
<h1>Manage Settings</h1>
</div>
<ul class="breadcrumb"><li class=""><a href="index">Dashboard</a></li><span class="divider"> / </span><li class="active"><a href="/settings">Settings</a></li></ul>
<ul class='nav nav-tabs'>
          <li title="" rel="" class="active">
            <a href="/settings">
              <i class="icon-th-list"></i>
              <span>Data Storage</span>
            </a>
          </li>
		  
          <li title="" rel="" class="">
            <a href="/settings">
              <i class="icon-th-list"></i>
              <span>Advanced Settings</span>
            </a>
          </li>
        
          <li title="" rel="" class="">
            <a href="/settings">
              <i class="icon-share"></i>
              <span>Export data</span>
            </a>
          </li>
          <li title="" rel="" class="">
            <a href="/settings">
              <i class="icon-arrow-down"></i>
              <span>Import data</span>
            </a>
          </li>
        
          <li title="" rel="" class="">
            <a href="/settings">
              <i class="icon-book"></i>
              <span>History</span>
            </a>
          </li>
	</div>
	
  <form class="form-horizontal">
    <fieldset>
    <div class="control-group">

          <!-- Select Basic -->
          <label class="control-label">Storage options</label>
          <div class="controls">
            <select class="input-xlarge">
			  <option>JSON</option>
			</select>
          </div>
        </div>
    <div class="control-group success">

          <!-- Text input-->
          <label class="control-label">Checksum</label>
          <div class="controls">
            <input type="text" value="<?php echo( $data->checksum( ) ) ?>" class="input-xlarge" disabled>
            <p class="help-block">data/storage.json</p>
          </div>
        </div>
		
		<div class="control-group">
          <!-- Button -->
          <div class="controls">
            <button class="btn btn-primary" type="submit">Submit</button>
          </div>
        </div>
    </fieldset>
  </form>


	</div>
	</div>
	</div>
	<div class="label label-info" id="loading" style="position:fixed; right:20px; bottom:20px; z-index:100000">Loading...</div>
	<script src="http://code.jquery.com/jquery-latest.js"></script>
	<script src="assets/js/bootstrap.min.js"></script>
	<script>
		$(window).load(function(){
			$('#loading').fadeOut();
		});
	</script>
</body>
</html>